from inventory import config
from inventory.app import create_app


if __name__ == '__main__':
    settings = {
        'SQLALCHEMY_DATABASE_URI': config.DATABASE_URL,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_ECHO': config.DEBUG

    }
    app = create_app(__name__, settings=settings)
    app.run(host='0.0.0.0', debug=config.DEBUG)
