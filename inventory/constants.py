EMPLOYEE_NOT_FOUND = 'Employee not found.'
RAW_MATERIAL_NOT_FOUND = 'Raw material not found.'
END_PRODUCT_NOT_FOUND = 'End product not found.'
DEFAULT_LIMIT = 20
DEFAULT_OFFSET = 0
