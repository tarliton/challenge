from http import HTTPStatus


class BaseException(Exception):
    def __init__(self, message, status_code=None):
        self.message = message
        self.status_code = status_code

    def __repr__(self):
        return self.message

    def to_dict(self):
        return {'error': self.message}


class DataNotFoundException(BaseException):
    def __init__(self, message, status_code=HTTPStatus.NOT_FOUND):
        self.message = message
        self.status_code = status_code
