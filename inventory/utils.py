from http import HTTPStatus

from flask import jsonify, make_response


def build_response(result, status=HTTPStatus.OK):
    if isinstance(result, list):
        result = {'results': result}

    return make_response(jsonify(result), status)
