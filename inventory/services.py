from inventory.constants import EMPLOYEE_NOT_FOUND, RAW_MATERIAL_NOT_FOUND, END_PRODUCT_NOT_FOUND
from inventory.enums import WorkingDayLengthEnum
from inventory.exceptions import DataNotFoundException
from inventory.models import EmployeeModel, RawMaterialModel, EndProductModel, recipes


class EmployeeService:
    def __init__(self, db):
        self.db = db

    def create(self, name, working_day_length):
        employee = EmployeeModel()
        employee.name = name
        employee.working_day_length = WorkingDayLengthEnum.to_timedelta(working_day_length)
        self.db.session.add(employee)
        self.db.session.commit()
        return employee.serialize()

    def update(self, employee_id, name=None, working_day_length=None):
        query = self.db.session.query(EmployeeModel)
        query = query.filter(EmployeeModel.id == employee_id)

        update_values = {}

        if name is not None:
            update_values['name'] = name

        if working_day_length is not None:
            working_day_length = WorkingDayLengthEnum.to_timedelta(working_day_length)
            update_values['working_day_length'] = working_day_length

        if update_values:
            query.update(update_values)
            self.db.session.commit()
            return employee_id

        return -1

    def delete(self, employee_id):
        self.db.session.query(EmployeeModel).filter(EmployeeModel.id == employee_id).delete()
        self.db.session.commit()

    def get(self, employee_id=None, limit=20, offset=0):
        query = self.db.session.query(EmployeeModel)
        if employee_id is not None:
            query = query.filter(EmployeeModel.id == employee_id)

        if limit is not None:
            query = query.limit(limit)

        if offset:
            query = query.offset(offset)

        result = [e.serialize() for e in query.all()]
        if employee_id is not None:
            if result:
                return result[0]
            else:
                raise DataNotFoundException(EMPLOYEE_NOT_FOUND)

        return result[offset: limit + offset]


class RawMaterialService:
    def __init__(self, db):
        self.db = db

    def create(self, name, quantity):
        raw_material = RawMaterialModel()
        raw_material.name = name
        raw_material.quantity = quantity
        self.db.session.add(raw_material)
        self.db.session.commit()
        return raw_material.serialize()

    def update(self, raw_material_id, name=None, quantity=None):
        query = self.db.session.query(RawMaterialModel)
        query = query.filter(RawMaterialModel.id == raw_material_id)

        update_values = {}

        if name is not None:
            update_values['name'] = name

        if quantity is not None:
            update_values['quantity'] = quantity

        if update_values:
            query.update(update_values)
            self.db.session.commit()

    def delete(self, raw_material_id):
        self.db.session.query(RawMaterialModel).filter(RawMaterialModel.id == raw_material_id)\
            .delete()

        self.db.session.commit()

    def get(self, raw_material_id=None, quantity_less_than=None, limit=20, offset=0):
        query = self.db.session.query(RawMaterialModel)
        if raw_material_id is not None:
            query = query.filter(RawMaterialModel.id == raw_material_id)

        if quantity_less_than is not None:
            query = query.filter(RawMaterialModel.quantity < quantity_less_than)

        if limit is not None:
            query = query.limit(limit)

        if offset:
            query = query.offset(offset)

        result = [e.serialize() for e in query.all()]
        if raw_material_id is not None:
            if result:
                return result[0]
            else:
                raise DataNotFoundException(RAW_MATERIAL_NOT_FOUND)

        return result[offset: limit + offset]


class EndProductService:
    def __init__(self, db):
        self.db = db

    def create(self, name, employee_id, recipe=None):
        recipe_items = recipe or []

        end_product = EndProductModel()
        end_product.name = name
        end_product.employee_id = employee_id
        raw_materials = self._get_raw_materials(recipe_items)
        end_product.recipe = raw_materials

        self.db.session.add(end_product)
        self.db.session.commit()
        return self._serialize_end_product([end_product])[0]

    def update(self, end_product_id, name=None, employee_id=None, recipe=None):
        query = self.db.session.query(EndProductModel)
        query = query.filter(EndProductModel.id == end_product_id)

        update_values = {}

        if name is not None:
            update_values['name'] = name

        if employee_id is not None:
            update_values['employee_id'] = employee_id

        if update_values:
            query.update(update_values)
            self.db.session.commit()

        if recipe is not None:
            query = self.db.session.query(EndProductModel)
            end_product = query.filter(EndProductModel.id == end_product_id).first()
            end_product.recipe = self._get_raw_materials(recipe)
            self.db.session.commit()

    def delete(self, end_product_id):
        self.db.session.query(EndProductModel).filter(EndProductModel.id == end_product_id)\
            .delete()
        self.db.session.commit()

    def get(self, end_product_id=None, limit=20, offset=0, raw_material=None, employee_name=None):
        query = self.db.session.query(EndProductModel)
        if end_product_id is not None:
            query = query.filter(EndProductModel.id == end_product_id)

        if raw_material is not None:
            query = query.join(recipes)
            query = query.join(RawMaterialModel)
            query = query.filter(RawMaterialModel.name == raw_material)

        if employee_name is not None:
            query = query.join(EmployeeModel)
            query = query.filter(EmployeeModel.name.ilike(f'%{employee_name}%'))

        if limit is not None:
            query = query.limit(limit)

        if offset:
            query = query.offset(offset)

        results = query.all()
        result = self._serialize_end_product(results)
        if end_product_id is not None:
            if result:
                return result[0]
            else:
                raise DataNotFoundException(END_PRODUCT_NOT_FOUND)

        return result[offset: limit + offset]

    def _get_raw_materials(self, raw_materials_ids):
        query = self.db.session.query(RawMaterialModel)
        query = query.filter(RawMaterialModel.id.in_(raw_materials_ids))
        return query.all()

    def _serialize_end_product(self, results):
        result = []
        for e in results:
            serialized_recipe = [r.serialize() for r in e.recipe]
            employee = e.employee.serialize()
            entity = e.serialize()
            entity['recipe'] = serialized_recipe
            entity['employee'] = employee
            result.append(entity)

        return result
