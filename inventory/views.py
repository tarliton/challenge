from http import HTTPStatus

from flask import jsonify, request, make_response
from flask.views import MethodView

from inventory.constants import DEFAULT_LIMIT, DEFAULT_OFFSET
from inventory.models import db
from inventory.services import EmployeeService, RawMaterialService, EndProductService
from inventory.utils import build_response


class BaseAPIView(MethodView):
    def __init__(self):
        self.service = self._service_cls()(db)

    def _parse_limit_offset(self):
        self.offset = int(request.args.get('offset', DEFAULT_OFFSET))
        self.limit = int(request.args.get('limit', DEFAULT_LIMIT))

    def _service_cls(self):
        raise NotImplemented('must be implemented')

    def _primary_key(self):
        raise NotImplemented('must be implemented')

    def _create_params(self):
        raise NotImplemented('must be implemented')

    def _filter_args(self):
        return []

    def get(self, **kwargs):
        self._parse_limit_offset()
        args = {k: v for k, v in request.args.items() if k in self._filter_args()}
        result = self.service.get(kwargs.get(self._primary_key()), limit=self.limit,
                                  offset=self.offset, **args)

        return build_response(result)

    def post(self):
        params = {k: v for k, v in request.json.items() if k in self._create_params()}
        entity = self.service.create(**params)
        return build_response(entity, status=HTTPStatus.CREATED)

    def patch(self, **kwargs):
        params = {k: v for k, v in request.json.items() if k in self._create_params()}
        entity_id = self.service.update(kwargs.get(self._primary_key()), **params)
        return jsonify({'ok': entity_id})

    def delete(self, **kwargs):
        self.service.delete(kwargs.get(self._primary_key()))
        return make_response('', HTTPStatus.NO_CONTENT)


class EmployeeAPIView(BaseAPIView):
    def _service_cls(self):
        return EmployeeService

    def _primary_key(self):
        return 'employee_id'

    def _create_params(self):
        return ['name', 'working_day_length']


class RawMaterialAPIView(BaseAPIView):
    def _service_cls(self):
        return RawMaterialService

    def _primary_key(self):
        return 'raw_material_id'

    def _create_params(self):
        return ['name', 'quantity']

    def _filter_args(self):
        return ['quantity_less_than']


class EndProductAPIView(BaseAPIView):
    def _service_cls(self):
        return EndProductService

    def _primary_key(self):
        return 'end_product_id'

    def _create_params(self):
        return ['name', 'employee_id', 'recipe']

    def _filter_args(self):
        return ['raw_material', 'employee_name']
