from sqlalchemy_utils import database_exists, create_database

from inventory.models import db


def create_tables(app):
    db_url = app.config['SQLALCHEMY_DATABASE_URI']
    if not database_exists(db_url):
        create_database(db_url)

    with app.app_context():
        db.init_app(app)
        db.create_all()


def drop_tables(app):
    with app.app_context():
        db.init_app(app)
        db.drop_all()
