from flask_sqlalchemy import SQLAlchemy

from inventory.serializers import RawMaterialSchema, EmployeSchema, EndProductSchema

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def serialize(self):
        return self.schema().dump(self)

    @property
    def schema(self):
        return self._schema()


class RawMaterialModel(BaseModel):
    __tablename__ = 'raw_materials'

    name = db.Column(db.String(100), nullable=False)
    quantity = db.Column(db.Integer, nullable=False)

    def _schema(self):
        return RawMaterialSchema


class EmployeeModel(BaseModel):
    __tablename__ = 'employees'

    name = db.Column(db.String(100), nullable=False)
    working_day_length = db.Column(db.Interval, nullable=False)
    end_products = db.relationship('EndProductModel', backref='employee', lazy=True)

    def _schema(self):
        return EmployeSchema


recipes = db.Table('recipes',
                   db.Column('end_product_id', db.Integer, db.ForeignKey('end_products.id'),
                             primary_key=True),
                   db.Column('raw_material_id', db.Integer, db.ForeignKey('raw_materials.id'),
                             primary_key=True),
                    db.Column('created_on', db.DateTime, default=db.func.now()),
                    db.Column('updated_on', db.DateTime, default=db.func.now(),
                              onupdate=db.func.now()),
                   )


class EndProductModel(BaseModel):
    __tablename__ = 'end_products'

    name = db.Column(db.String(100), nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employees.id'), nullable=False)

    recipe = db.relationship('RawMaterialModel', secondary=recipes, lazy='subquery',
                             backref=db.backref('pages', lazy=True))

    def _schema(self):
        return EndProductSchema
