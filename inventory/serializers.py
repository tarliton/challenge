from marshmallow import Schema, fields


class RawMaterialSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    quantity = fields.Int()


class EmployeSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    working_day_length = fields.TimeDelta(precision='hours')


class Recipeschema(Schema):
    end_product_id = fields.Int()
    raw_material_id = fields.Str()


class EndProductSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    employee = fields.Nested(EmployeSchema)
    recipe = fields.Nested(Recipeschema, many=True)
