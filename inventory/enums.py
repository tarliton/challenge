from enum import Enum
from datetime import timedelta


class WorkingDayLengthEnum(Enum):
    FOUR_HOURS = 4
    SIX_HOURS = 6
    EIGHT_HOURS = 8

    @staticmethod
    def to_timedelta(length):
        working_day_length = WorkingDayLengthEnum(length)
        return timedelta(hours=working_day_length.value)
