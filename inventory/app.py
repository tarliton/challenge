from flask import Flask

from inventory.blueprints import employee_resource, raw_material_resource, end_products_resource
from inventory.database import create_tables
from inventory.error_handlers import inventory_error_handler
from inventory.exceptions import BaseException
from inventory.models import db


class Inventory(Flask):
    pass


def create_app(app_name=None, settings=None):
    settings = settings or {}
    app_name = app_name if app_name else __name__
    app = Inventory(app_name)
    app.register_error_handler(BaseException, inventory_error_handler)
    app.config.update(**settings)
    create_tables(app)
    app.register_blueprint(employee_resource)
    app.register_blueprint(raw_material_resource)
    app.register_blueprint(end_products_resource)
    db.init_app(app)
    return app
