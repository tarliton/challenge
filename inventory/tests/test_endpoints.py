import json
from unittest import TestCase

import pytest

from inventory.app import Inventory
from inventory.blueprints import employee_resource, raw_material_resource, end_products_resource
from inventory.error_handlers import inventory_error_handler
from inventory.exceptions import BaseException
from inventory.models import db


@pytest.fixture(scope='class')
def client(request):
    app = Inventory(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.register_blueprint(employee_resource)
    app.register_blueprint(raw_material_resource)
    app.register_blueprint(end_products_resource)
    app.register_error_handler(BaseException, inventory_error_handler)
    app.config['TESTING'] = True
    client = app.test_client()

    request.cls.client = client
    request.cls.db = db
    request.cls.app = app
    yield client


@pytest.mark.usefixtures("client")
class BaseTestCase(TestCase):
    def setUp(self):
        with self.app.app_context():
            self.db.init_app(self.app)
            self.db.create_all()

    def tearDown(self):
        with self.app.app_context():
            self.db.session.remove()
            self.db.drop_all()

    def _create_employee(self, name, working_day_length):
        data = dict(name=name, working_day_length=working_day_length)
        return self.client.post('/employees', data=json.dumps(data),
                                content_type='application/json')

    def _update_employee(self, employee_id, name=None, working_day_length=None):
        data = dict(name=name, working_day_length=working_day_length)
        data = {k: v for k, v in data.items() if v is not None}
        return self.client.patch(f'/employees/{employee_id}', data=json.dumps(data),
                                 content_type='application/json')

    def _create_raw_material(self, name, quantity):
        data = dict(name=name, quantity=quantity)
        return self.client.post('/raw_materials', data=json.dumps(data),
                                content_type='application/json')

    def _update_raw_material(self, raw_material_id, name=None, quantity=None):
        data = dict(name=name, quantity=quantity)
        data = {k: v for k, v in data.items() if v is not None}
        return self.client.patch(f'/raw_materials/{raw_material_id}', data=json.dumps(data),
                                 content_type='application/json')

    def _create_end_product(self, name, employee_id, recipe=None):
        recipe = recipe or []
        data = dict(name=name, employee_id=employee_id, recipe=recipe)
        return self.client.post('/end_products', data=json.dumps(data),
                                content_type='application/json')

    def _update_end_product(self, end_product_id, name=None, employee_id=None, recipe=None):
        data = dict(name=name, employee_id=employee_id, recipe=recipe)
        data = {k: v for k, v in data.items() if v is not None}
        return self.client.patch(f'/end_products/{end_product_id}', data=json.dumps(data),
                                 content_type='application/json')


class EmployeeTestCase(BaseTestCase):
    def test_empty_db(self):
        result = self.client.get('/employees').json
        self.assertEqual(result, {'results': []})

    def test_get_employee(self):
        employee_id = self._create_employee('João', 6).json['id']
        result = self.client.get(f'/employees/{employee_id}')
        employee = result.json

        self.assertEqual(result.status_code, 200)
        self.assertEqual(employee['name'], 'João')
        self.assertEqual(employee['working_day_length'], 6)

    def test_insert_employee(self):
        result = self._create_employee('João', 6)
        self.assertEqual(result.status_code, 201)
        self.assertEqual(result.json['name'], 'João')
        self.assertEqual(result.json['working_day_length'], 6)

    def test_update_employee(self):
        employee_id = self._create_employee('João', 6).json['id']
        self._update_employee(employee_id, name='João Pedro')
        employee = self.client.get(f'/employees/{employee_id}').json

        self.assertEqual(employee['name'], 'João Pedro')
        self.assertEqual(employee['working_day_length'], 6)

    def test_delete_employee(self):
        employee_id = self._create_employee('João', 6).json['id']
        result = self.client.delete(f'/employees/{employee_id}')
        self.assertEqual(result.status_code, 204)

        employee_not_found = self.client.get(f'/employees/{employee_id}').json
        self.assertEqual(employee_not_found['error'], 'Employee not found.')


class RawMaterialTestCase(BaseTestCase):
    def test_empty_db(self):
        result = self.client.get('/raw_materials').json
        self.assertEqual(result, {'results': []})

    def test_get_raw_material(self):
        raw_material_id = self._create_raw_material('Queijo', 20).json['id']
        result = self.client.get(f'/raw_materials/{raw_material_id}')
        raw_material = result.json

        self.assertEqual(result.status_code, 200)
        self.assertEqual(raw_material['name'], 'Queijo')
        self.assertEqual(raw_material['quantity'], 20)

    def test_insert_raw_material(self):
        result = self._create_raw_material('Queijo', 10)
        self.assertEqual(result.status_code, 201)
        self.assertEqual(result.json['name'], 'Queijo')
        self.assertEqual(result.json['quantity'], 10)

    def test_update_raw_material(self):
        raw_material_id = self._create_raw_material('Queijo', 15).json['id']
        self._update_raw_material(raw_material_id, name='Quejo Minas')
        raw_material = self.client.get(f'/raw_materials/{raw_material_id}').json

        self.assertEqual(raw_material['name'], 'Quejo Minas')
        self.assertEqual(raw_material['quantity'], 15)

    def test_delete_raw_material(self):
        raw_material_id = self._create_raw_material('Queijo', 6).json['id']
        result = self.client.delete(f'/raw_materials/{raw_material_id}')
        self.assertEqual(result.status_code, 204)

        raw_material_not_found = self.client.get(f'/raw_materials/{raw_material_id}').json
        self.assertEqual(raw_material_not_found['error'], 'Raw material not found.')


class EndProductTestCase(BaseTestCase):

    def test_empty_db(self):
        result = self.client.get('/end_products').json
        self.assertEqual(result, {'results': []})

    def test_get_end_product(self):
        employee = self._create_employee('João', 6).json
        end_product_id = self._create_end_product('Pão de queijo', employee['id']).json['id']
        result = self.client.get(f'/end_products/{end_product_id}')
        end_product = result.json

        self.assertEqual(result.status_code, 200)
        self.assertEqual(end_product['name'], 'Pão de queijo')
        self.assertEqual(end_product['employee']['id'], employee['id'])
        self.assertEqual(end_product['recipe'], [])

    def test_insert_end_product(self):
        employee = self._create_employee('João', 6).json

        item_1 = self._create_raw_material('Margarina', 10).json
        item_2 = self._create_raw_material('Manteiga', 5).json
        item_3 = self._create_raw_material('Sal', 50).json

        result = self._create_end_product('Pão de queijo', employee['id'],
                                          recipe=[item_1['id'], item_2['id'], item_3['id']])

        self.assertEqual(result.status_code, 201)
        self.assertEqual(result.json['name'], 'Pão de queijo')
        self.assertEqual(result.json['employee']['id'], 1)
        self.assertEqual(result.json['recipe'], [item_1, item_2, item_3])

    def test_update_end_product(self):
        employee = self._create_employee('João', 6).json

        item_1 = self._create_raw_material('Margarina', 10).json
        item_2 = self._create_raw_material('Manteiga', 5).json
        item_3 = self._create_raw_material('Sal', 50).json

        result = self._create_end_product('Pão de queijo', employee['id'],
                                          recipe=[item_1['id'], item_2['id'], item_3['id']])

        self._update_end_product(result.json['id'], name='Bolo de queijo')
        end_product = self.client.get(f'/end_products/{result.json["id"]}').json

        self.assertEqual(end_product['name'], 'Bolo de queijo')
        self.assertEqual(end_product['employee']['id'], 1)
        self.assertEqual(end_product['recipe'], [item_1, item_2, item_3])

    def test_update_end_product_recipe(self):
        employee = self._create_employee('João', 6).json

        item_1 = self._create_raw_material('Margarina', 10).json
        item_2 = self._create_raw_material('Manteiga', 5).json
        item_3 = self._create_raw_material('Sal', 50).json
        item_4 = self._create_raw_material('Pimenta', 50).json
        item_5 = self._create_raw_material('Ovo', 50).json

        result = self._create_end_product('Pão de queijo', employee['id'],
                                          recipe=[item_1['id'], item_2['id'], item_3['id']])

        self._update_end_product(result.json['id'], recipe=[item_4['id'], item_5['id']])
        end_product = self.client.get(f'/end_products/{result.json["id"]}').json

        self.assertEqual(end_product['name'], 'Pão de queijo')
        self.assertEqual(result.json['employee']['id'], 1)
        self.assertEqual(sorted(end_product['recipe'], key=lambda x: x['id']),
                         sorted([item_4, item_5], key=lambda x: x['id']))

    def test_delete_end_product(self):
        employee = self._create_employee('João', 6).json

        item_1 = self._create_raw_material('Margarina', 10).json
        item_2 = self._create_raw_material('Manteiga', 5).json
        item_3 = self._create_raw_material('Sal', 50).json

        end_product = self._create_end_product('Pão de queijo', employee['id'],
                                               recipe=[item_1['id'], item_2['id'],
                                                       item_3['id']]).json

        result = self.client.delete(f'/end_products/{end_product["id"]}')
        self.assertEqual(result.status_code, 204)

        end_product_not_found = self.client.get(f'/end_products/{end_product["id"]}').json
        self.assertEqual(end_product_not_found['error'], 'End product not found.')
