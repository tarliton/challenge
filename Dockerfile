FROM python:3.6.6-alpine3.6

WORKDIR /app

RUN apk add --update build-base postgresql-dev

RUN pip install pipenv

ADD Pipfile.lock /app
ADD Pipfile /app

RUN pipenv install --system

ADD . /app

CMD [ "python", "main.py" ]

EXPOSE 5000
