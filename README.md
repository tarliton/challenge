# Inventory API

API para cadastro de Funcionários, Produtos e Máterias-prima.

# Desenvolvimento

Para executar este projeto é necessário ter o Python 3.6 instalado em sua máquina.


Instalando as dependências:


O projeto gerencia suas dependências com o pipenv, portanto, basta executar:  
- `pip install pipenv`  
- `pipenv shell`  
- `pipenv install`  


Para inicar o servidor:  
- configurar um arquivo com varíaveis de ambiente `.env` seguindo o exemplo de `.env.example`  
- executar `python main.py`  
- ou com docker:  
  -  `docker build -t inventory .`  
  -  `docker run -it --env-file=.env -p 5000:5000 inventory`  


# Documentação dos endpoints


| Resource                         | Methods            | Attributes                                | Return        | Filters                                    |
|----------------------------------|--------------------|-------------------------------------------|---------------|--------------------------------------------|
| /raw_materials                   | GET, POST          | name: str, quantity: int                  | 200, 201      | limit, offset, quantity_less_than          |
| /raw_materials/<raw_material_id> | GET, PATCH, DELETE | name: str, quantity: int                  | 200, 200, 204 |                                            |
| /employees                       | GET, POST          | name: str, working_day_length: int        | 200, 201      | limit, offset                              |
| /employees/<employee_id>         | GET, PATCH, DELETE | name: str, working_day_length: int        | 200, 200, 204 |                                            |
| /end_products                    | GET, POST          | name: str, employee_id: int, recipe: list | 200, 201      | limit, offset, raw_material, employee_name |
| /end_products/<end_product_id>   | GET, PATCH, DELETE | name: str, employee_id: int, recipe: list | 200, 200, 204 |                                            |



Exemplos:

```bash
curl --header "Content-Type: application/json" --request POST --data '{"name":"João Marcos","working_day_length": 6}'  http://localhost:5000/employees

curl --header "Content-Type: application/json" --request POST --data '{"name":"Maria Joaquina","working_day_length": 8}'  http://localhost:5000/employees

curl --header "Content-Type: application/json" --request POST --data '{"name":"Fermento (Tipo 1)","quantity": 10}'  http://localhost:5000/raw_materials

curl --header "Content-Type: application/json" --request POST --data '{"name":"Açúcar","quantity": 4}'  http://localhost:5000/raw_materials

curl --header "Content-Type: application/json" --request POST --data '{"name":"Manteiga","quantity": 6}'  http://localhost:5000/raw_materials

curl --header "Content-Type: application/json" --request POST --data '{"name":"Fermento (Tipo 2)","quantity": 2}'  http://localhost:5000/raw_materials

curl --header "Content-Type: application/json" --request POST  --data '{"name":"Macarrão Instânteneo","employee_id": 2, "recipe": [1,3]}'  http://localhost:5000/end_products

curl --header "Content-Type: application/json" --request POST  --data '{"name":"Macarrão Integral","employee_id": 1, "recipe": [4]}'  http://localhost:5000/end_products
```
