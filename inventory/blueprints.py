from flask import Blueprint

from inventory.views import EmployeeAPIView, RawMaterialAPIView, EndProductAPIView

employee_resource = Blueprint('employee_resource', __name__)

employee_resource.add_url_rule('/employees/<employee_id>',
                               view_func=EmployeeAPIView.as_view('specific_employees'),
                               methods=['GET', 'PATCH', 'DELETE'])

employee_resource.add_url_rule('/employees', view_func=EmployeeAPIView.as_view('employees'),
                               methods=['GET', 'POST'])

raw_material_resource = Blueprint('raw_material_resource', __name__)

raw_material_resource.add_url_rule('/raw_materials/<raw_material_id>',
                                   view_func=RawMaterialAPIView.as_view('specific_raw_materials'),
                                   methods=['GET', 'PATCH', 'DELETE'])

raw_material_resource.add_url_rule('/raw_materials',
                                   view_func=RawMaterialAPIView.as_view('raw_materials'),
                                   methods=['GET', 'POST'])

end_products_resource = Blueprint('end_products_resource', __name__)

end_products_resource.add_url_rule('/end_products/<end_product_id>',
                                   view_func=EndProductAPIView.as_view('specific_end_products'),
                                   methods=['GET', 'PATCH', 'DELETE'])

end_products_resource.add_url_rule('/end_products',
                                   view_func=EndProductAPIView.as_view('end_products'),
                                   methods=['GET', 'POST'])
