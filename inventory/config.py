from decouple import config


DEBUG = config('INV_DEBUG', default=False, cast=bool)
DATABASE_URL = config('INV_DATABASE_URL', default='sqlite://')
